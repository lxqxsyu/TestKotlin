package guohe.testkotlin;

import org.junit.Test;

import java.util.List;

import guohe.testkotlin.block.account.entry.TestBean;
import guohe.testkotlin.block.account.model.AccountModelImpl;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func2;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        new AccountModelImpl().requestTest(new Response.OnResult<List<TestBean>>() {
            @Override
            public void result(List<TestBean> response) {
                System.out.println("response:" + response.get(0).getBanner_img());
            }
        });
    }

    private void testRxJava(){
        Observable.range(1, 5).scan(new Func2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer integer) {
                return sum + integer;
            }
        }).subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Integer integer) {

            }
        });

    }
}