package guohe.testkotlin;


import com.taotong.baseapp.log.XLog;
import com.taotong.baseapp.mvp.model.BaseResultBean;
import com.taotong.baseapp.xbase.XToast;

import rx.Subscriber;

/**
 * Created by 水寒 on 2017/7/14.
 * 接口请求结果返回处理
 */

public class Response{

    public static final int RESULT_SUCCESS = 1;   //请求成功
    public static final int RESULT_MESSAGE = 2;   //提示消息
    public static final int RESULT_SYS_ERR = 3;   //系统异常

    public static abstract class OnResult<V> {
        public abstract void result(V response);
        public void onFaild(String faildMsg){
            XToast.showToast(faildMsg);
        }
        public void completed(){ }
    }

    public static class ModelResult<T extends BaseResultBean> extends Subscriber<T> {

        private OnResult mResult;

        public ModelResult(OnResult result){
            mResult = result;
        }

        @Override
        public void onCompleted() {
            //TODO 做一些统一的完成操作
            mResult.completed();
        }

        @Override
        public void onError(Throwable e) {
            //TODO 做一些统一的错误处理
            XLog.e("请求接口出错 == " + e.getMessage());
            e.printStackTrace();
            mResult.completed();
        }

        @Override
        public void onNext(T response) {
            switch (response.getStatus()){
                case RESULT_SUCCESS:
                    if(response.getInfo() == null){
                        mResult.result(response);
                    }else{
                        mResult.result(response.getInfo());
                    }
                    break;
                case RESULT_MESSAGE:
                    mResult.onFaild(response.getMsg());
                    break;
                case RESULT_SYS_ERR:
                    mResult.onFaild("系统错误");
                    break;
                default:
                    mResult.onFaild("未知错误");
                    break;
            }
        }
    }
}
