package guohe.testkotlin.bus;

import com.alibaba.android.arouter.facade.Postcard;
import com.taotong.baseapp.rxbus.BaseBusEvent;

/**
 * Created by 水寒 on 2017/11/2.
 */

public class RouteCallbackBus extends BaseBusEvent {

    private Enum type;
    private Postcard postcard;

    public RouteCallbackBus(Enum type, Postcard postcard) {
        this.type = type;
        this.postcard = postcard;
    }

    public Enum getType() {
        return type;
    }

    public Postcard getPostcard() {
        return postcard;
    }
}
