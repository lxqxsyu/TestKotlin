package guohe.testkotlin;

import com.taotong.baseapp.mvp.model.BaseResultListBean;
import com.taotong.baseapp.mvp.model.BaseResultObjectBean;

import java.util.Map;

import guohe.testkotlin.block.account.entry.TestBean;
import guohe.testkotlin.block.account.entry.UserInfo;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by 水寒 on 2017/10/31.
 * 接口定义
 */

public interface API{

    /**
     * 测试
     * @param params
     * @return
     */
    @FormUrlEncoded
    @POST("static/gethomebanner")
    Observable<BaseResultListBean<TestBean>> postGetBanner(@FieldMap Map<String, Object> params);

    /**
     * 登录接口
     * @param params
     * @return
     */
    @FormUrlEncoded
    @POST("user/login")
    Observable<BaseResultObjectBean<UserInfo>> postAccountLogin(@FieldMap Map<String, Object> params);
}
