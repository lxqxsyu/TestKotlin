package guohe.testkotlin.route;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;

/**
 * Created by 水寒 on 2017/11/2.
 * 登录检查过滤器
 */
@Interceptor(priority = 1, name = "检查是否登录")
public class LoginCheckIntercepter implements IInterceptor{

    boolean logined = true;
    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {
        if(logined){
            callback.onContinue(postcard);
        }else{
            postcard.setTag("您还未登录");
            callback.onInterrupt(new RuntimeException("未登录异常"));
        }
    }

    // 拦截器的初始化，会在sdk初始化的时候调用该方法，仅会调用一次
    @Override
    public void init(Context context) {

    }
}
