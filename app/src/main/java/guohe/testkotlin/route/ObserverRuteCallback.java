package guohe.testkotlin.route;


import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.taotong.baseapp.rxbus.RxBus;

import guohe.testkotlin.bus.RouteCallbackBus;

/**
 * Created by 水寒 on 2017/11/2.
 */

public class ObserverRuteCallback implements NavigationCallback {

    public enum RouteCallBackType{
        typeFound,
        typeLost,
        typeArrival,
        typeInterrupt
    }

    @Override
    public void onFound(Postcard postcard) {
        RxBus.getDefault().post(new RouteCallbackBus(
                RouteCallBackType.typeFound, postcard));
    }

    @Override
    public void onLost(Postcard postcard) {
        RxBus.getDefault().post(new RouteCallbackBus(
                RouteCallBackType.typeLost, postcard));
    }

    @Override
    public void onArrival(Postcard postcard) {
        RxBus.getDefault().post(new RouteCallbackBus(
                RouteCallBackType.typeArrival, postcard));
    }

    @Override
    public void onInterrupt(Postcard postcard) {
        RxBus.getDefault().post(new RouteCallbackBus(
                RouteCallBackType.typeInterrupt, postcard));
    }
}
