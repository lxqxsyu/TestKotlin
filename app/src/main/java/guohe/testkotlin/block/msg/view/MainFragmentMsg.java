package guohe.testkotlin.block.msg.view;

import android.view.View;

import com.taotong.baseapp.mvp.IPresenter;

import guohe.testkotlin.base.BaseMainFragment;

/**
 * Created by 水寒 on 2017/11/1.
 */

public class MainFragmentMsg extends BaseMainFragment implements IFragmentMsgView{

    @Override
    protected int getContentView() {
        return 0;
    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView(View view) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }
}
