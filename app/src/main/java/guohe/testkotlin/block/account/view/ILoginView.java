package guohe.testkotlin.block.account.view;

import com.taotong.baseapp.mvp.IView;

/**
 * Created by 水寒 on 2017/10/31.
 */

public interface ILoginView extends IView{

    void showLoginSuccessDialog();
}
