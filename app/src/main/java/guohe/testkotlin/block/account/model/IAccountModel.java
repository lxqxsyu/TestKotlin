package guohe.testkotlin.block.account.model;

import com.taotong.baseapp.mvp.IModel;

import java.util.List;

import guohe.testkotlin.Response;
import guohe.testkotlin.block.account.entry.TestBean;
import guohe.testkotlin.block.account.entry.UserInfo;

/**
 * Created by 水寒 on 2017/10/31.
 * 账号相关model
 */

public interface IAccountModel extends IModel{

    /**
     * 请求账号密码登录
     * @param username
     * @param pwd
     * @param result
     */
    void requestAccoutLogin(String username, String pwd, Response.OnResult<UserInfo> result);

    void requestTest(Response.OnResult<List<TestBean>> result);
}
