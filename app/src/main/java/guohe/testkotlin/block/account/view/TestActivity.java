package guohe.testkotlin.block.account.view;

import com.taotong.baseapp.mvp.IPresenter;

import guohe.testkotlin.base.BaseActivity;

/**
 * Created by 水寒 on 2017/11/2.
 */

public class TestActivity extends BaseActivity {

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }

    @Override
    protected int getContentView() {
        return 0;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initData() {

    }
}
