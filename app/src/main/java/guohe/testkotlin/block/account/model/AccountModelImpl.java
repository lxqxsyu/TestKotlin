package guohe.testkotlin.block.account.model;

import com.taotong.baseapp.http.RequestParam;
import com.taotong.baseapp.mvp.model.BaseResultBean;
import com.taotong.baseapp.mvp.model.BaseResultListBean;

import java.util.List;

import guohe.testkotlin.Response;
import guohe.testkotlin.block.account.entry.TestBean;
import guohe.testkotlin.block.account.entry.UserInfo;
import guohe.testkotlin.manager.ModelManager;

/**
 * Created by 水寒 on 2017/10/31.
 * 账号model实现类
 */

public class AccountModelImpl implements IAccountModel{

    @Override
    public void requestAccoutLogin(String username, String pwd, Response.OnResult<UserInfo> result) {
        RequestParam requestParam = new RequestParam.Builder()
                .setParam("username", username)
                .setParam("pwd", pwd)
                .builder();
        ModelManager.requestApiOnResult(ModelManager.API.postAccountLogin(requestParam.getParams()),
                new Response.ModelResult<BaseResultBean<UserInfo>>(result));
    }

    @Override
    public void requestTest(Response.OnResult<List<TestBean>> result) {
        RequestParam requestParam = new RequestParam.Builder()
                .builder();
        ModelManager.requestApiOnResult(ModelManager.API.postGetBanner(requestParam.getParams()),
                new Response.ModelResult<BaseResultListBean<TestBean>>(result));
    }
}
