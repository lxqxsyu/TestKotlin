package guohe.testkotlin.block.account.entry;

import com.taotong.baseapp.mvp.model.BaseBean;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class TestBean extends BaseBean{

    private String banner_title; //	string	banner标题
    private String banner_desc; //	string	banner描述
    private String banner_img; //	string	banner图片
    private String banner_link; //	string	banner详情h5页面
    private String banner_button_ishave; //	string	是否有跳转按钮
    private String banner_button_desc; //	string	跳转按钮文案
    private String banner_type; //	string	['liveroom','group','coursedetail','campaign']
    private String banner_content_id; //	string	[liveroom对应的：liveroom_userid,group对应的：group_id,coursedetail对应的：course_id]

    public String getBanner_title() {
        return banner_title;
    }

    public void setBanner_title(String banner_title) {
        this.banner_title = banner_title;
    }

    public String getBanner_desc() {
        return banner_desc;
    }

    public void setBanner_desc(String banner_desc) {
        this.banner_desc = banner_desc;
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public String getBanner_link() {
        return banner_link;
    }

    public void setBanner_link(String banner_link) {
        this.banner_link = banner_link;
    }

    public String getBanner_button_ishave() {
        return banner_button_ishave;
    }

    public void setBanner_button_ishave(String banner_button_ishave) {
        this.banner_button_ishave = banner_button_ishave;
    }

    public String getBanner_button_desc() {
        return banner_button_desc;
    }

    public void setBanner_button_desc(String banner_button_desc) {
        this.banner_button_desc = banner_button_desc;
    }

    public String getBanner_type() {
        return banner_type;
    }

    public void setBanner_type(String banner_type) {
        this.banner_type = banner_type;
    }

    public String getBanner_content_id() {
        return banner_content_id;
    }

    public void setBanner_content_id(String banner_content_id) {
        this.banner_content_id = banner_content_id;
    }
}
