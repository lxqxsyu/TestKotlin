package guohe.testkotlin.block.account.presenter;

import com.taotong.baseapp.mvp.IView;

import java.util.List;

import guohe.testkotlin.Response;
import guohe.testkotlin.block.account.entry.TestBean;
import guohe.testkotlin.block.account.model.AccountModelImpl;
import guohe.testkotlin.block.account.view.ILoginView;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class LoginPresenterImpl implements ILoginPresenter{

    private AccountModelImpl mAccountModel;
    private ILoginView mLoginView;

    public LoginPresenterImpl(){
        mAccountModel = new AccountModelImpl();
    }

    @Override
    public void requestAccountLogin(String account, String pwd) {

    }

    @Override
    public void requestTest() {
        mAccountModel.requestTest(new Response.OnResult<List<TestBean>>() {
            @Override
            public void result(List<TestBean> response) {
                mLoginView.showLoginSuccessDialog();
            }
        });
    }

    @Override
    public void attachView(IView mvpView) {
        mLoginView = (ILoginView) mvpView;
    }

    @Override
    public void dettachView() {
        mLoginView = null;
    }
}
