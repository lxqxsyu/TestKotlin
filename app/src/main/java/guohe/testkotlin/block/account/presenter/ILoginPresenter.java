package guohe.testkotlin.block.account.presenter;

import com.taotong.baseapp.mvp.IPresenter;

/**
 * Created by 水寒 on 2017/10/31.
 */

public interface ILoginPresenter extends IPresenter{

    /**
     * 请求账号密码登录
     * @param account
     * @param pwd
     */
    void requestAccountLogin(String account, String pwd);

    void requestTest();
}
