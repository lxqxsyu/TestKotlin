package guohe.testkotlin.block.account.view;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.taotong.baseapp.mvp.IPresenter;
import com.taotong.baseapp.xbase.XToast;

import guohe.testkotlin.R;
import guohe.testkotlin.base.BaseActivity;
import guohe.testkotlin.block.account.presenter.ILoginPresenter;
import guohe.testkotlin.block.account.presenter.LoginPresenterImpl;

/**
 * Created by 水寒 on 2017/10/31.
 */

@Route(path = "/account/LoginActivity")
public class LoginActivity extends BaseActivity implements ILoginView {

    private ILoginPresenter mLoginPresenter;

    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        XToast.showToast(getIntent().getStringExtra("username")
                + ":" + getIntent().getIntExtra("age", 0));
    }

    @Override
    protected IPresenter initPresenter() {
        mLoginPresenter = new LoginPresenterImpl();
        return mLoginPresenter;
    }

    @Override
    protected void initData() {
        mLoginPresenter.requestTest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.dettachView();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }

    @Override
    public void showLoginSuccessDialog() {
        XToast.showToast("登录成功。。。");
    }

}
