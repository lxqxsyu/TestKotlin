package guohe.testkotlin.block;

import com.taotong.baseapp.mvp.IPresenter;

import guohe.testkotlin.R;
import guohe.testkotlin.base.BaseActivity;

/**
 * Created by 水寒 on 2017/11/1.
 * 引导页
 */

public class GuideActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_guide;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initData() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }
}
