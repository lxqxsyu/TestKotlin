package guohe.testkotlin.block;

import android.os.Message;
import android.os.SystemClock;

import com.taotong.baseapp.mvp.IPresenter;
import com.taotong.baseapp.weak.WeakRefrenceHandler;

import guohe.testkotlin.R;
import guohe.testkotlin.base.BaseActivity;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class SplashActivity extends BaseActivity{

    private static final int SPLASH_TIME = 3000;        //闪屏时间
    private static final int HAND_START_INIT = 0x0001;  //开始初始化操作
    private static final int HAND_TURN_NEXT = 0x0002;   //跳转到下一个界面
    public static String mapFilePath;
    private long mStartTime;  //开始启动页时间

    private WeakRefrenceHandler<SplashActivity> mHandler = new MyWeakRefrenceHandler(this);

    static class MyWeakRefrenceHandler extends WeakRefrenceHandler<SplashActivity>{

        public MyWeakRefrenceHandler(SplashActivity ref) {
            super(ref);
        }

        @Override
        protected void handleMessage(SplashActivity ref, Message msg) {
            switch (msg.what){
                case HAND_START_INIT:
                    ref.mStartTime = SystemClock.elapsedRealtime();
                    ref.doInit();
                    int remainDelay =(int)(SPLASH_TIME -
                            (SystemClock.elapsedRealtime() - ref.mStartTime));
                    if(remainDelay <= 0){
                        ref.turnToOtherView();
                    }else{
                        ref.mHandler.sendEmptyMessageDelayed(HAND_TURN_NEXT, remainDelay);
                    }
                    break;
                case HAND_TURN_NEXT:
                    ref.turnToOtherView();
                    break;
            }
        }
    }


    /**
     * 做一些初始化操作
     */
    private void doInit(){

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            mHandler.sendEmptyMessage(HAND_START_INIT);
        }
    }

    public void turnToOtherView() {
        MainActivity.startActivity(SplashActivity.this);
        SplashActivity.this.finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }
}
