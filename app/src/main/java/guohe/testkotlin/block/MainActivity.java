package guohe.testkotlin.block;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.taotong.baseapp.imageloader.ILFactory;
import com.taotong.baseapp.imageloader.NetImageView;
import com.taotong.baseapp.mvp.IPresenter;

import guohe.testkotlin.R;
import guohe.testkotlin.base.BaseActivity;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class MainActivity extends BaseActivity{

    private TextView mTextMessage;
    private NetImageView mTestImagView;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        mTextMessage = getView(R.id.message);
        BottomNavigationView navigation = getView(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getView(R.id.button_turn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/account/LoginActivity")
                        .withString("username", "lixiaoqiang")
                        .withInt("age", 18)
                        .navigation();
                //ARouter.getInstance().build("/account/LoginActivity").navigation();
                /*ARouter.getInstance().build("/account/LoginActivity").navigation(
                        MainActivity.this, new ObserverRuteCallback());*/
            }
        });

        getView(R.id.button_play_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/video/PlayVideoActivity").navigation();
            }
        });

        mTestImagView = getView(R.id.test_net_imageview);

        ILFactory.getLoader().loadRes(mTestImagView, R.mipmap.ic_launcher);
    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void customeToolbar(TextView titleText, TextView toolbarMenu, ImageButton moreButton) {
        titleText.setText("自定义toolbar");
    }

    @Override
    protected void initData() {

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }

    };

    public static void startActivity(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }
}
