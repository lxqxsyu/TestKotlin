package guohe.testkotlin.block.video;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.taotong.baseapp.mvp.IPresenter;

import java.util.ArrayList;
import java.util.List;

import guohe.testkotlin.R;
import guohe.testkotlin.base.BaseActivity;

/**
 * Created by 水寒 on 2017/11/2.
 */

@Route(path = "/video/PlayVideoActivity")
public class PlayVideoActivity extends BaseActivity{

    public final static String IMG_TRANSITION = "IMG_TRANSITION";
    public final static String TRANSITION = "TRANSITION";

    private VideoPlayer mVideoPlayer;
    private OrientationUtils mOrientationUtils;
    private boolean isTransition;

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(int Code, String msg) {

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_play_video;
    }

    @Override
    protected void initView() {
        mVideoPlayer = getView(R.id.video_player);

        isTransition = false;
        String source1 = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4";
        String name = "普通";
        SwitchVideoModel switchVideoModel = new SwitchVideoModel(name, source1);

        String source2 = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f30.mp4";
        String name2 = "清晰";
        SwitchVideoModel switchVideoModel2 = new SwitchVideoModel(name2, source2);

        List<SwitchVideoModel> list = new ArrayList<>();

        list.add(switchVideoModel);
        list.add(switchVideoModel2);

        mVideoPlayer.setUp(list, true, "测试视频");

        //设置返回键
        mVideoPlayer.getBackButton().setVisibility(View.VISIBLE);

        //设置旋转
        mOrientationUtils = new OrientationUtils(this, mVideoPlayer);

        //设置全屏按键功能,这是使用的是选择屏幕，而不是全屏
        mVideoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrientationUtils.resolveByClick();
            }
        });

        //是否可以滑动调整
        mVideoPlayer.setIsTouchWiget(true);

        //设置返回按键功能
        mVideoPlayer.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //过渡动画
        initTransition();
    }

    @Override
    public void onBackPressed() {
        //先返回正常状态
        if (mOrientationUtils.getScreenType() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            mVideoPlayer.getFullscreenButton().performClick();
            return;
        }
        //释放所有
        mVideoPlayer.setStandardVideoAllCallBack(null);
        GSYVideoPlayer.releaseAllVideos();
        if (isTransition && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.onBackPressed();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            }, 500);
        }
    }

    @Override
    protected IPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initData() {

    }

    private void initTransition() {
        if (isTransition && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
            ViewCompat.setTransitionName(mVideoPlayer, IMG_TRANSITION);
            startPostponedEnterTransition();
        } else {
            mVideoPlayer.startPlayLogic();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoPlayer.onVideoPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoPlayer.onVideoResume();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mOrientationUtils != null)
            mOrientationUtils.releaseListener();
    }
}
