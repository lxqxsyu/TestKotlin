package guohe.testkotlin.base;

import com.taotong.baseapp.xbase.XBaseFragment;

import guohe.testkotlin.manager.RefreshManager;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by 水寒 on 2017/10/31.
 * 基础Fragment
 */

public abstract class BaseFragment extends XBaseFragment{

    private PtrFrameLayout mRefreshView;

    /**
     * 刷新界面
     * @param id
     * @param onRefresh
     */
    protected PtrFrameLayout refreshView(int id, RefreshManager.OnRefresh onRefresh) {
        final PtrFrameLayout refreshView = getView(id);
        if (refreshView == null) return null;
        mRefreshView = refreshView;
        RefreshManager.refreshView(this.getContext(), refreshView, onRefresh);
        return refreshView;
    }

    public void refreshStop() {
        if(mRefreshView == null) return;
        mRefreshView.refreshComplete();
    }
}
