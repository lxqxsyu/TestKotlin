package guohe.testkotlin.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jaeger.library.StatusBarUtil;
import com.taotong.baseapp.xbase.XBaseActivity;

import guohe.testkotlin.R;
import guohe.testkotlin.manager.RefreshManager;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by 水寒 on 2017/10/31.
 * 基础Activity
 */

public abstract class BaseActivity extends XBaseActivity{

    private Toolbar mToolbar;
    private PtrFrameLayout mRefreshView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        setStatuBar();
    }

    private void initToolbar(){
        mToolbar = getView(R.id.toolbar);
        if(mToolbar == null) return;
        setSupportActionBar(mToolbar);
        /*ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }*/
        mToolbar.findViewById(R.id.toolbar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.this.finish();
            }
        });
        TextView toolbarTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        ImageButton moreButton = (ImageButton) mToolbar.findViewById(R.id.toolbar_more);
        TextView toolbarMenu = (TextView) mToolbar.findViewById(R.id.toolbar_menu);
        customeToolbar(toolbarTitle, toolbarMenu, moreButton);
    }

    protected void customeToolbar(TextView titleText, TextView toolbarMenu, ImageButton moreButton) {

    }

    protected void setStatuBar(){
        StatusBarUtil.setColor(this, getResources().getColor(R.color.colorPrimaryDark));
    }

    /**
     * 刷新界面
     * @param id
     * @param onRefresh
     */
    protected void refreshView(int id, RefreshManager.OnRefresh onRefresh) {
        final PtrFrameLayout refreshView = getView(id);
        if (refreshView == null) return;
        mRefreshView = refreshView;
        RefreshManager.refreshView(this, refreshView, onRefresh);
    }

    public void refreshStop() {
        if(mRefreshView == null) return;
        mRefreshView.refreshComplete();
    }
}
