package guohe.testkotlin;

import android.app.Application;

import com.alibaba.android.arouter.launcher.ARouter;
import com.taotong.baseapp.CommonConfig;

import static com.taotong.baseapp.CommonConfig.isDebug;

/**
 * Created by 水寒 on 2017/10/31.
 * 自定义application
 */

public class CustomeApplication extends Application {

    private static CustomeApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        CommonConfig.init(this, BuildConfig.DEBUG);
        //初始化路由
        if(isDebug()){
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);
    }

    public static CustomeApplication getInstance(){
        return mInstance;
    }
}
