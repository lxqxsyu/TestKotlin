package guohe.testkotlin.manager;

import com.taotong.baseapp.http.RetrofitManage;

import guohe.testkotlin.API;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 水寒 on 2017/10/31.
 * 接口数据请求工具类
 */

public class ModelManager {

    public static API API = RetrofitManage.getInstance().getRetrofit().create(API.class);

    public static <T> Observable requestApiOnResult(Observable<T> observable, Subscriber<? super T> subscriber){
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
        return observable;
    }
}
