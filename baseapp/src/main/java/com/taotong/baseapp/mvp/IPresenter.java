package com.taotong.baseapp.mvp;

/**
 * Created by 水寒 on 2017/10/31.
 */

public interface IPresenter<V extends IView> {

    /**
     * 将视图依附到展示器（Presenter）
     * @param mvpView
     */
    void attachView(V mvpView);

    /**
     * 解除依附关系
     */
    void dettachView();
}
