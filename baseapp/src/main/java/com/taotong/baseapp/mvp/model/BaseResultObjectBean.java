package com.taotong.baseapp.mvp.model;

/**
 * Created by shuihan on 2017/2/9.
 * 普通对象
 */

public class BaseResultObjectBean<T extends BaseBean> extends BaseResultBean<T> {

    private T info;

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }
}
