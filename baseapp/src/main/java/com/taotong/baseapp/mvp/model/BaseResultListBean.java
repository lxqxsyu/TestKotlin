package com.taotong.baseapp.mvp.model;

import java.util.List;

/**
 * Created by 水寒 on 2017/8/29.
 */

public class BaseResultListBean<T extends BaseBean> extends BaseResultBean<T> {

    private List<T> info;

    public List<T> getInfo() {
        return info;
    }

    public void setInfo(List<T> info) {
        this.info = info;
    }
}
