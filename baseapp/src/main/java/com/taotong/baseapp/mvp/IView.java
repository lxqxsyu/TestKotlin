package com.taotong.baseapp.mvp;

/**
 * Created by 水寒 on 2017/10/31.
 */

public interface IView {

    /**
     * 显示loading
     */
    void showLoading();
    /**
     *  显示错误
     */
    void showError(int Code, String msg);
}
