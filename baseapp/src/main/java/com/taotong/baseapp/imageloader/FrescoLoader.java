package com.taotong.baseapp.imageloader;

import com.taotong.baseapp.imageloader.fresco.FrescoUtils;

/**
 * Created by 水寒 on 2017/10/31.
 * Fresco图片加载
 */

public class FrescoLoader implements ILoader{

    @Override
    public void loadUrl(NetImageView imageView, String url){
        loadUrl(imageView, url, 0, 0);
    }

    @Override
    public void loadUrl(NetImageView imageView, String url, int maxWidth, int maxHeight) {
        FrescoUtils.loadUrl(imageView, url, maxWidth, maxHeight);
    }

    @Override
    public void loadRes(NetImageView imageView, int resId){
        loadRes(imageView, resId, 0, 0);
    }

    @Override
    public void loadRes(NetImageView imageView, int resId, int maxWidth, int maxHeight) {
        FrescoUtils.loadRes(imageView, resId, null, maxWidth, maxHeight, null);
    }
}
