package com.taotong.baseapp.imageloader;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class GlideLoader implements ILoader{

    @Override
    public void loadUrl(NetImageView imageView, String url) {

    }

    @Override
    public void loadRes(NetImageView imageView, int resId) {

    }

    @Override
    public void loadUrl(NetImageView imageView, String url, int maxWidth, int maxHeight) {

    }

    @Override
    public void loadRes(NetImageView imageView, int resId, int maxWidth, int maxHeight) {

    }
}
