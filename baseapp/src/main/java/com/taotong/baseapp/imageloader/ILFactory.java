package com.taotong.baseapp.imageloader;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class ILFactory {

    private static ILoader loader;

    public static ILoader getLoader() {
        if (loader == null) {
            synchronized (ILFactory.class) {
                if (loader == null) {
                    loader = new FrescoLoader();
                }
            }
        }
        return loader;
    }
}
