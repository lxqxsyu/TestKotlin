package com.taotong.baseapp.imageloader;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.taotong.baseapp.R;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class NetImageView extends SimpleDraweeView {

    public NetImageView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
        init(context, null);
    }

    public NetImageView(Context context) {
        super(context);
        init(context, null);
    }

    public NetImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NetImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public NetImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray gdhAttrs = context.obtainStyledAttributes(attrs, R.styleable.NetImageAttr);

        GenericDraweeHierarchy builder = getHierarchy();
        try {
            final int indexCount = gdhAttrs.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                final int attr = gdhAttrs.getIndex(i);
                // most popular ones first
                if (attr == R.styleable.NetImageAttr_XscaleType) {
                    builder.setActualImageScaleType(getScaleTypeFromXml(gdhAttrs, attr));
                } else if (attr == R.styleable.NetImageAttr_XdefaultImg) {
                    builder.setPlaceholderImage(getDrawable(context, gdhAttrs, attr));
                }
            }
            setHierarchy(builder);
        } finally {
            gdhAttrs.recycle();
        }
    }

    @Nullable
    private static Drawable getDrawable(
            Context context,
            TypedArray gdhAttrs,
            int attrId) {
        int resourceId = gdhAttrs.getResourceId(attrId, 0);
        return (resourceId == 0) ? null : context.getResources().getDrawable(resourceId);
    }

    @Nullable
    private static ScalingUtils.ScaleType getScaleTypeFromXml(
            TypedArray gdhAttrs,
            int attrId) {
        switch (gdhAttrs.getInt(attrId, -2)) {
            case -1: // none
                return null;
            case 0: // fitXY
                return ScalingUtils.ScaleType.FIT_XY;
            case 1: // fitStart
                return ScalingUtils.ScaleType.FIT_START;
            case 2: // fitCenter
                return ScalingUtils.ScaleType.FIT_CENTER;
            case 3: // fitEnd
                return ScalingUtils.ScaleType.FIT_END;
            case 4: // center
                return ScalingUtils.ScaleType.CENTER;
            case 5: // centerInside
                return ScalingUtils.ScaleType.CENTER_INSIDE;
            case 6: // centerCrop
                return ScalingUtils.ScaleType.CENTER_CROP;
            case 7: // focusCrop
                return ScalingUtils.ScaleType.FOCUS_CROP;
            default:
                // this method is supposed to be called only when XML attribute is specified.
                throw new RuntimeException("XML attribute not specified!");
        }
    }

}
