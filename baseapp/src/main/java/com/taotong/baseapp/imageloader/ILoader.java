package com.taotong.baseapp.imageloader;

/**
 * Created by 水寒 on 2017/10/31.
 * 图片加载接口
 */

public interface ILoader {

    /**
     * 此方法不建议使用
     * @param imageView
     * @param url
     */
    void loadUrl(NetImageView imageView, String url);

    /**
     * 此方法不建议使用
     * @param imageView
     * @param resId
     */
    void loadRes(NetImageView imageView, int resId);

    /**
     * 加载网络图片
     * @param imageView
     * @param url
     * @param maxWidth
     * @param maxHeight
     */
    void loadUrl(NetImageView imageView, String url, int maxWidth, int maxHeight);

    /**
     * 加载本地图片
     * @param imageView
     * @param resId
     * @param maxWidth
     * @param maxHeight
     */
    void loadRes(NetImageView imageView, int resId, int maxWidth, int maxHeight);

}
