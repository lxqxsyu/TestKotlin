package com.taotong.baseapp.http;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

import static com.taotong.baseapp.CommonConfig.DEFAULT_SERVER_HTTP;
import static com.taotong.baseapp.CommonConfig.SERVER_PATH;

/**
 * Created by shuihan on 2016/12/8.
 */

public class RetrofitManage {

    private static Retrofit mRetrofit;
    private static OkHttpClient mOkHttpClient;

    private static RetrofitManage mInstance;

    private RetrofitManage(){
        mOkHttpClient = OkHttpManager.getInstance().getOkHttpClient();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(DEFAULT_SERVER_HTTP + SERVER_PATH)
                .client(mOkHttpClient)
                .addConverterFactory(ToStringConverterFactory.create())
                .addConverterFactory(ToGsonConverterFactory.create())
                //.addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public static RetrofitManage getInstance(){
        if(mInstance == null){
            mInstance = new RetrofitManage();
        }
        return mInstance;
    }

    public static void refreshHttpByConfig(){
        mInstance = new RetrofitManage();
    }

    /**
     * 获取Retrofit对象
     *
     * @return
     */
    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}
