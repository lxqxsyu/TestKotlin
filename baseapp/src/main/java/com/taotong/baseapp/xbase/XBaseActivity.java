package com.taotong.baseapp.xbase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.taotong.baseapp.log.XLog;
import com.taotong.baseapp.mvp.IPresenter;
import com.taotong.baseapp.mvp.IView;
import com.taotong.baseapp.rxbus.BaseBusEvent;
import com.taotong.baseapp.rxbus.RxBus;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by 水寒 on 2017/10/31.
 */

public abstract class XBaseActivity extends AppCompatActivity implements IView{

    private List<Subscription> mSubscriptions = new ArrayList<>();
    private IPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        initView();
        mPresenter = initPresenter();
        if(mPresenter != null) {
            mPresenter.attachView(this);
        }
        initData();
    }

    @SuppressWarnings("unchecked")
    public final <E extends View> E getView (int id) {
        try {
            return (E) findViewById(id);
        } catch (ClassCastException ex) {
            XLog.e("Could not cast View to concrete class.", ex);
            throw ex;
        }
    }

    /**
     * 设置布局文件
     * @return
     */
    protected abstract int getContentView();

    /**
     * 初始化View
     */
    protected abstract void initView();

    /**
     * 初始化Presenter
     * @return
     */
    protected abstract IPresenter initPresenter();

    /**
     * 初始化数据
     */
    protected abstract void initData();


    protected <E extends BaseBusEvent> void observerRxBus(Class<E> busClass, final Action1<E> onNext) {
        Subscription subscription = RxBus.getDefault().observerRxBus(busClass, onNext);
        mSubscriptions.add(subscription);
    }

    protected <E extends BaseBusEvent> void observerRxBusSticky(Class<E> busClass, final Action1<E> onNext) {
        Subscription subscription = RxBus.getDefault().observerRxBusSticky(busClass, onNext);
        mSubscriptions.add(subscription);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPresenter != null){
            mPresenter.dettachView();
        }
    }
}
