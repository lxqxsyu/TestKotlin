package com.taotong.baseapp.xbase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taotong.baseapp.log.XLog;
import com.taotong.baseapp.mvp.IPresenter;
import com.taotong.baseapp.mvp.IView;
import com.taotong.baseapp.rxbus.BaseBusEvent;
import com.taotong.baseapp.rxbus.RxBus;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by 水寒 on 2017/10/31.
 */

public abstract class XBaseFragment extends Fragment implements IView{

    protected View mView;
    private List<Subscription> mSubscriptions = new ArrayList<>();
    private IPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(getContentView(), null);
        initView(mView);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = initPresenter();
        if(mPresenter != null) {
            mPresenter.attachView(this);
        }
        initData();
    }

    /**
     * 设置布局文件
     * @return
     */
    protected abstract int getContentView();

    /**
     * 初始化Presenter
     * @return
     */
    protected abstract IPresenter initPresenter();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化视图
     * @param view
     */
    protected abstract void initView(View view);

    protected <E extends BaseBusEvent> void observerRxBus(Class<E> busClass, final Action1<E> onNext) {
        Subscription subscription = RxBus.getDefault().observerRxBus(busClass, onNext);
        mSubscriptions.add(subscription);
    }

    protected <E extends BaseBusEvent> void observerRxBusSticky(Class<E> busClass, final Action1<E> onNext) {
        Subscription subscription = RxBus.getDefault().observerRxBusSticky(busClass, onNext);
        mSubscriptions.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(Subscription subscription : mSubscriptions){
            if(subscription == null) continue;
            if(subscription.isUnsubscribed()) continue;
            subscription.unsubscribe();
        }
        mSubscriptions.clear();
    }

    @SuppressWarnings("unchecked")
    protected final <E extends View> E getView (int id) {
        try {
            return (E) mView.findViewById(id);
        } catch (ClassCastException ex) {
            XLog.e("Could not cast View to concrete class.", ex);
            throw ex;
        }
    }
}
