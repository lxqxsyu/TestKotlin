package com.taotong.baseapp.xbase;

import android.widget.Toast;

import com.taotong.baseapp.CommonConfig;
import com.wou.commonutils.StringUtils;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class XToast {

    /**
     * 显示一个toast到界面上
     * @param message
     */
    public static void showToast(String message){
        if(StringUtils.isEmpty(message)) return;
        Toast.makeText(CommonConfig.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
