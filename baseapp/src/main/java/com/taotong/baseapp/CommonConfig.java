package com.taotong.baseapp;

import android.app.Application;
import android.content.Context;

import com.taotong.baseapp.imageloader.fresco.FrescoUtils;

/**
 * Created by 水寒 on 2017/10/31.
 */

public class CommonConfig {

    public static final String DEFAULT_TAG = "TEST";

    //默认服务器路径
    public static final String DEFAULT_SERVER_HTTP = "http://api.aawork.org/";

    public static final String SERVER_PATH = "v3/";

    private static boolean isDebug;
    private static Context context;

    /**
     * 初始化框架
     * @param application
     */
    public static void init(Application application, boolean isDebug){
        CommonConfig.context = application.getApplicationContext();
        CommonConfig.isDebug = isDebug;
        //初始化Fresco
        FrescoUtils.init(context, 1024);  //1G缓存
    }

    /**
     * 判断是否是debug
     * @return
     */
    public static boolean isDebug() {
        return isDebug;
    }

    /**
     * 获取context
     * @return
     */
    public static Context getContext(){
        return context;
    }

}
